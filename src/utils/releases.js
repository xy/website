let _releases;

export const fetchReleases = async () => {
	if (!_releases) {
		const response = await fetch('https://codeberg.org/api/v1/repos/forgejo/forgejo/releases');
		_releases = await response.json();
	}
	return _releases;
};

export const getLatestRelease = async () => {
	const releases = await fetchReleases();
	return releases[0];
}
