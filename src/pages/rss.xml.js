import rss from '@astrojs/rss';

import { SITE, BLOG } from '~/config.mjs';
import { fetchPosts } from '~/utils/posts';
import { getPermalink } from '~/utils/permalinks';

export const get = async () => {
	const posts = await fetchPosts();

	return rss({
		title: `Forgejo News`,
		description: SITE.description,
		site: import.meta.env.SITE,

		items: posts.map((post) => ({
			link: getPermalink(post.slug, 'post'),
			title: post.title,
			description: post.description || post.excerpt,
			pubDate: post.publishDate,
		})),

		stylesheet: import.meta.env.BASE_URL + 'pretty-feed-v3.xsl',
	});
};
