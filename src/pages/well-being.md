---
layout: '~/layouts/PageLayout.astro'
publishDate: 'Aug 08 2022'
title: 'Code of Conduct and Well Being team'
---

# Well Being and Moderation teams

Temporary Well Being and Moderation teams [were appointed 10 November 2022](https://codeberg.org/forgejo/meta/issues/13).

The moderation team will rely on this [Code of Conduct](/code-of-conduct/) when diplomacy fails.

## [Well Being team](https://codeberg.org/org/forgejo/teams/well-being)

Its goal is to defuse tensions.

It has no power whatsover. The members are approved by the organization and trusted to:

- Read all communications to detect tensions between people before they escalate.
- Do their best to defuse tensions.

## [Moderation team](https://codeberg.org/org/forgejo/teams/moderation)

Its goal is to enforce the [Code of Conduct](/code-of-conduct/) when diplomacy fails.

It has the power to exclude people from a space.

Its decisions must be logical, fact based and transparent to the organization trusting them with the task.
